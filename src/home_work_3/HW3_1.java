package home_work_3;

import java.util.Scanner;
import java.util.Arrays;

public class HW3_1{
	public static void main (String args[]) {
		int arr[] = {4, 2, 5, 1, 3};
		System.out.println(sumArray(arr));
		arrReverse();
		ArithmeticMean();
		System.out.println(Arrays.toString(minMaxReverse(arr)));
		System.out.println(Arrays.toString(bubleSort(arr)));
		System.out.println(arrToNum(arr));

	}

	public static int sumArray (int array[]){
		int result = 0;
		for(int i = 0; i < array.length; i++){
			result += array[i];
		}
		return result;
	}

	public static void arrReverse(){
		Scanner scanner = new Scanner(System.in);
		System.out.print("Enter the number of numbers: ");
		int n = scanner.nextInt();
		int array[] = new int[n];
		int var = 0;

		for (int i = 0; i < array.length; i++){
			System.out.print("Enter the number: ");
			array[i] = scanner.nextInt();	
		}
		System.out.println(Arrays.toString(array));
		for (int i = 0; i < array.length/2; i++){
			var = array[i];
			array[i] = array[array.length - i - 1];
			array[array.length - i - 1] = var;
		}
		System.out.println(Arrays.toString(array));
	}

	public static void ArithmeticMean() {
		Scanner scanner = new Scanner(System.in);
		System.out.print("Enter the number of numbers: ");
		int n = scanner.nextInt();
		int array[] = new int[n];
		int result = 0;

		for (int i = 0; i < array.length; i++){
			System.out.print("Enter the number: ");
			array[i] = scanner.nextInt();	
		}

		result = sumArray(array)/array.length;
		System.out.println(Arrays.toString(array));
		System.out.println(result);
	}

	public static int[] minMaxReverse(int array[]) {
		int result[] = array;
		int max = result[0];
		int max_i = 0;
		int min = result[0];
		int min_i = 0;
		for (int i = 1; i < result.length; i++ ) {
			 if(max < result[i]){
			 	max = result[i];
			 	max_i = i;
			 }
			 if (min > result[i]){
			 	min = result[i];
			 	min_i = i;
			 }
		}
		System.out.println(max);
		System.out.println(min);
		result[max_i] = min;
		result[min_i] = max;
		return result;
	}

	public static int arrToNum(int array[]) {
		int number = 0;
		number = array[0];
		for (int i = 1; i < array.length ;i++ ) {
			number = number*10+array[i];
		}
		return number;
	}

	public static int[] bubleSort(int[] array){
		int result[] = array;
		int var = 0;
		for (int i = 0; i < result.length ;i++ ) {
			for (int j = 0;j < result.length - i-1 ;j++ ) {
				if (result[j+1] < result[j]){
					var = array[j];
					result[j] = result[j + 1];
					result[j + 1] = var;
				}
				
			}
			
		}
		return result;
	}


}