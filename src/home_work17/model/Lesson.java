package home_work17.model;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class Lesson {

    private Long lessonId;

    private final Calendar lessonData;

    private final String nameLesson;

    private Course course;


    public Lesson(String nameLesson, int day, int month, int year) {
        this.nameLesson = nameLesson;
        this.lessonData = new GregorianCalendar(year, month, day);
    }

    public Lesson(Long lessonId, String nameLesson, int day, int month, int year) {
        this.lessonId = lessonId;
        this.nameLesson = nameLesson;
        this.lessonData = new GregorianCalendar(year, month, day);
    }

    public Long getLessonId() {
        return lessonId;
    }

    public void setLessonId(Long lessonId) {
        this.lessonId = lessonId;
    }

    public Calendar getLessonData() {
        return lessonData;
    }

    public String getNameLesson() {
        return nameLesson;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public Course getCourse() {
        return course;
    }

    public int getDay() {
        return lessonData.get(Calendar.DAY_OF_MONTH);
    }

    public int getMouth() {
        return lessonData.get(Calendar.MONTH);
    }

    public int getYear() {
        return lessonData.get(Calendar.YEAR);
    }

    @Override
    public String toString() {
        return "Lesson{" +
                "Lesson Name= " + nameLesson +
                " lessonId= " + lessonId +
                " day= " + getDay() +
                " mouth= " + getMouth() +
                " year= " + getYear() +
                "}\r\n";
    }
}
