package home_work17.model;

import java.util.List;

public class Teacher {

    private Long teacherId;

    private final String firstName;

    private final String lastName;

    private final Integer experience;

    private List<Course> courses;

    public Teacher(Long teacherId, String firstName, String lastName, Integer experience) {
        this(firstName, lastName, experience);
        this.teacherId = teacherId;
    }

    public Teacher(String firstName, String lastName, Integer experience) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.experience = experience;
    }

    public String getFirstName() {
        return firstName;
    }

    public Long getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(Long teacherId) {
        this.teacherId = teacherId;
    }

    public void setCourses(List<Course> courses) {
        this.courses = courses;
    }

    public List<Course> getCourses() {
        return courses;
    }

    public String getLastName() {
        return lastName;
    }

    public Integer getExperience() {
        return experience;
    }

    @Override
    public String toString() {
        return "Teacher{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", experience=" + experience +
                "}\r\n";
    }

}
