package home_work17.model;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class Course {

    private Long UUID;

    private final String courseName;

    private final Calendar startData;

    private final Calendar endData;

    private List<Teacher> teachers;

    private List<Lesson> lessons;


    public Course(Long uuid, String courseName, Calendar startData, Calendar endData) {
        this(courseName, startData, endData);
        this.UUID = uuid;

    }

    public Course(String courseName, Calendar startData, Calendar endData) {
        this.courseName = courseName;
        this.startData = startData;
        this.endData = endData;
    }

    public Course(String courseName, Calendar startData, Calendar endData, List<Teacher> teachers, List<Lesson> lessons) {
        this(courseName, startData, endData);
        this.teachers = teachers;
        this.lessons = lessons;
    }


    public Long getUUID() {
        return UUID;
    }

    public String getCourseName() {
        return courseName;
    }

    public String getStartData() {
        return startData.get(Calendar.DAY_OF_MONTH) + " " +
                startData.get(Calendar.MONTH) + " " +
                startData.get(Calendar.YEAR);
    }

    public String getEndData() {
        return endData.get(Calendar.DAY_OF_MONTH) + " " +
                endData.get(Calendar.MONTH) + " " +
                endData.get(Calendar.YEAR);
    }

    public List<Teacher> getTeachers() {
        return teachers;
    }

    public List<Lesson> getLessons() {
        return lessons;
    }

    public void setUUID(Long UUID) {
        this.UUID = UUID;
    }

    public void setTeachers(List<Teacher> teachers) {
        this.teachers = teachers;
    }

    public void setLessons(List<Lesson> lessons) {
        this.lessons = lessons;
    }

    @Override
    public String toString() {
        return "Course{" +
                "UUID=" + UUID +
                ", courseName='" + courseName + '\'' +
                ", startData=" + getStartData() +
                ", endData=" + getEndData() +
                "}\r\n";
    }
}

