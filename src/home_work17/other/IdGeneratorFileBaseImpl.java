package home_work17.other;

import java.io.*;

public class IdGeneratorFileBaseImpl implements IdGenerator {

    private String fileName;

    public IdGeneratorFileBaseImpl(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public Long nextId() {
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
            String lastGeneratedIdAsString = bufferedReader.readLine();
            long newId = Long.parseLong(lastGeneratedIdAsString);
            newId++;
            bufferedReader.close();
            BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
            writer.write(Long.toString(newId));
            writer.close();
            return newId;
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    public Long getId() {
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
            String lastGeneratedIdAsString = bufferedReader.readLine();
            long newId = Long.parseLong(lastGeneratedIdAsString);
            bufferedReader.close();
            return newId;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return -1L;
    }
}
