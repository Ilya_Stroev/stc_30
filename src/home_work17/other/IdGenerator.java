package home_work17.other;

public interface IdGenerator {
    Long nextId();
}
