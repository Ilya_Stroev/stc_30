package home_work17.other;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class OtherMain {
    public static void main(String[] args) {
        BufferedReader bf = null;
        BufferedWriter bw = null;

        try {
            bw = new BufferedWriter(new FileWriter("new.txt", true));
            bw.write("привет мир\r\n");
            bw.write("Иногда программирование это боль!\r\n");
            bw.close();
            bf = new BufferedReader(new FileReader("ew.txt"));
            String current = bf.readLine();
            while (current != null) {
                System.out.println(current);
                current = bf.readLine();
            }
            bf.close();


        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
