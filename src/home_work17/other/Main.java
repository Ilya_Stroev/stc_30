package home_work17.other;

import home_work17.DAO.*;
import home_work17.model.Course;
import home_work17.model.Lesson;
import home_work17.model.Teacher;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        Random re = new Random();

        IdGeneratorFileBaseImpl idGeneratorFileBase = new IdGeneratorFileBaseImpl("idLesson.txt");

        LessonDao lessonDao = new LessonDaoFileBaseImpl("lesson.txt", idGeneratorFileBase, "course.txt");
////Раскоментируй если хочешь добавить Уроки!
//        lessonDao.save(new Lesson("Lesson"+idGeneratorFileBase.getId(),re.nextInt(30), re.nextInt(11),2021));
//        lessonDao.save(new Lesson("Lesson"+idGeneratorFileBase.getId(),re.nextInt(30), re.nextInt(11),2021));
//        lessonDao.save(new Lesson("Lesson"+idGeneratorFileBase.getId(),re.nextInt(30), re.nextInt(11),2021));
//        lessonDao.save(new Lesson("Lesson"+idGeneratorFileBase.getId(),re.nextInt(30), re.nextInt(11),2021));
//        lessonDao.save(new Lesson("Lesson"+idGeneratorFileBase.getId(),re.nextInt(30), re.nextInt(11),2021));
////
////        Optional<Lesson> les1 = lessonDao.find("Lesson3");
////        Lesson lesson = les1.get();
////        System.out.println(lesson.toString());

        IdGeneratorFileBaseImpl idGeneratorFileBase1 = new IdGeneratorFileBaseImpl("idTeacher.txt");

        TeacherDao teacherDao = new TeacherDaoFileBaseImpl("teacher.txt", idGeneratorFileBase1, "course.txt");
////Раскоментируй если хочешь добавить Учителей!
//        teacherDao.save(new Teacher("TeacherF"+idGeneratorFileBase1.getId(),"LastName", re.nextInt(40)));
//        teacherDao.save(new Teacher("TeacherF"+idGeneratorFileBase1.getId(),"LastName", re.nextInt(40)));
//        teacherDao.save(new Teacher("TeacherF"+idGeneratorFileBase1.getId(),"LastName", re.nextInt(40)));
//        teacherDao.save(new Teacher("TeacherF"+idGeneratorFileBase1.getId(),"LastName", re.nextInt(40)));
//        teacherDao.save(new Teacher("TeacherF"+idGeneratorFileBase1.getId(),"LastName", re.nextInt(40)));
//
//        Optional<Teacher> teacher1 = teacherDao.find("TeacherF5");
//        Teacher teacher = teacher1.get();
//        System.out.println(teacher.toString());
//


        IdGeneratorFileBaseImpl idGeneratorFileBase3 = new IdGeneratorFileBaseImpl("idCourse.txt");

        CourseDao courseDao = new CourseDaoFileBasedImpl("course.txt", "teacher.txt", "lesson.txt", idGeneratorFileBase3);
//////Раскоментируй если хочешь добавить Курс!
//        List<Teacher> teachers = teacherDao.getAll();
//        List<Lesson> lessons = lessonDao.getAll();
//
//        List<Teacher> teachers1 = teachers.subList(0, 2);
//        List<Lesson> lessons1 = lessons.subList(0, 1);
//
//        List<Teacher> teachers2 = teachers.subList(0, re.nextInt(20));
//        List<Lesson> lessons2 = lessons.subList(0, re.nextInt(20));
//
//        List<Teacher> teachers3 = teachers.subList(4, 10);
//        List<Lesson> lessons3 = lessons.subList(0, 5);
//
//        Calendar start = new GregorianCalendar();
//        start.set(Calendar.DAY_OF_MONTH, re.nextInt(30));
//        start.set(Calendar.MONTH, re.nextInt(11));
//        start.set(Calendar.YEAR, 2020);
//        Calendar end = new GregorianCalendar();
//        end.set(Calendar.DAY_OF_MONTH, re.nextInt(30));
//        end.set(Calendar.MONTH, re.nextInt(11));
//        end.set(Calendar.YEAR, 2021);
//
//        courseDao.save(new Course("Course" + idGeneratorFileBase3.getId(), start, end, teachers2, lessons2));

        Optional<Course> course = courseDao.find(58L);
        Course crs = course.get();
        System.out.println(crs.toString());
        System.out.println(crs.getLessons().toString());

        Optional<Course> course1 = courseDao.find(36L);
        Course crs1 = course1.get();
        System.out.println(crs1.toString());
        System.out.println(crs1.getTeachers().toString());

        Optional<Teacher> oTeacher = teacherDao.find(8L);
        Teacher teacher = oTeacher.get();
        System.out.println(teacher.toString());
        System.out.println(teacher.getCourses().toString());

        Optional<Teacher> oTeacher1 = teacherDao.find(5L);
        Teacher teacher1 = oTeacher1.get();
        System.out.println(teacher1.toString());
        System.out.println(teacher1.getCourses().toString());

        Optional<Lesson> oLesson = lessonDao.find(5L);
        Lesson lesson = oLesson.get();
        System.out.println(lesson.toString());
        System.out.println(lesson.getCourse().toString());

    }
}
