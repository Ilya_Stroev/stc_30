package home_work17.DAO;

import home_work17.model.Course;
import home_work17.model.Teacher;

public interface TeacherDao extends OurDao<Teacher> {
}
