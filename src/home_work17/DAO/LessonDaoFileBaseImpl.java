package home_work17.DAO;

import home_work17.model.Course;
import home_work17.model.Lesson;
import home_work17.other.IdGenerator;

import java.io.*;
import java.util.*;

public class LessonDaoFileBaseImpl implements LessonDao {

    private String fileName;

    private IdGenerator idGenerator;

    private String courseFileName;

    public LessonDaoFileBaseImpl(String fileName, IdGenerator idGenerator, String courseFileName) {
        this(fileName);
        this.idGenerator = idGenerator;
        this.courseFileName = courseFileName;
    }

    public LessonDaoFileBaseImpl(String fileName) {
        this.fileName = fileName;

    }

    private Mapper<Lesson, String> lessonToStringMapper = lesson ->
            lesson.getLessonId() + " " +
                    lesson.getNameLesson() + " " +
                    lesson.getDay() + " " +
                    lesson.getMouth() + " " +
                    lesson.getYear() + "\r\n";

    private Mapper<String, Lesson> stringToLessonMapper = string -> {
        String data[] = string.split(" ");
        return new Lesson(Long.parseLong(data[0]),
                data[1],
                Integer.parseInt(data[2]),
                Integer.parseInt(data[3]),
                Integer.parseInt(data[4]));
    };

    @Override
    public Optional<Lesson> find(Long key) {
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
            String current = bufferedReader.readLine();

            while (current != null) {
                String data[] = current.split(" ");

                String existedId = data[0];

                if (Long.parseLong(existedId) == key) {
                    Lesson lesson = stringToLessonMapper.map(current);
                    Long id = searchId(key, "lessonId_courseId.txt");
                    CourseDao courseDao = new CourseDaoFileBasedImpl(courseFileName);
                    Course course = filteredById(courseDao.getAll(), id);
                    lesson.setCourse(course);
                    return Optional.of(lesson);
                }

                current = bufferedReader.readLine();
            }

            bufferedReader.close();
            return Optional.empty();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    private Course filteredById(List<Course> all, Long id) {
        for (Course c : all
        ) {
            if (c.getUUID().equals(id)) {
                return c;
            }
        }
        return null;
    }


    @Override
    public void save(Lesson lesson) {
        try {
            lesson.setLessonId(idGenerator.nextId());
            BufferedWriter keyLesson = new BufferedWriter(new FileWriter("lessonId_courseId.txt", true));
            keyLesson.write(lesson.getLessonId() + " \r\n");
            BufferedWriter outputStream = new BufferedWriter(new FileWriter(fileName, true));
            outputStream.write(lessonToStringMapper.map(lesson));
            outputStream.close();
            keyLesson.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public List<Lesson> getAll() {
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
            String current = bufferedReader.readLine();
            List<Lesson> lessons = new ArrayList<>();

            while (current != null) {
                String data[] = current.split(" ");
                Lesson lesson = stringToLessonMapper.map(current);
                lessons.add(lesson);
                current = bufferedReader.readLine();
            }

            bufferedReader.close();
            return lessons;
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

    }

    private Long searchId(Long id, String fileName) {
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
            String current = bufferedReader.readLine();

            while (current != null) {
                String data[] = current.split(" ");

                if (data.length == 2) {
                    Long existedId = Long.parseLong(data[0]);
                    if (existedId.equals(id)) {
                        return Long.parseLong(data[1]);
                    }
                }
                current = bufferedReader.readLine();
            }
            bufferedReader.close();
            return -1L;
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }


}
