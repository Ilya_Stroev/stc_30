package home_work17.DAO;

import home_work17.model.Course;
import home_work17.model.Lesson;
import home_work17.model.Teacher;
import home_work17.other.IdGenerator;

import java.io.*;
import java.util.*;

public class CourseDaoFileBasedImpl implements CourseDao {

    private String fileName;

    private String teacherFileName;

    private String lessonFileName;

    private IdGenerator idGenerator;

    public CourseDaoFileBasedImpl(String fileName, String teacherFileName, String lessonFileName, IdGenerator idGenerator) {
        this.fileName = fileName;
        this.teacherFileName = teacherFileName;
        this.lessonFileName = lessonFileName;
        this.idGenerator = idGenerator;
    }

    public CourseDaoFileBasedImpl(String fileName) {
        this.fileName = fileName;
    }

    private Mapper<Course, String> courseToStringMapper = course ->
            course.getUUID() + " " +
                    course.getCourseName() + " " +
                    course.getStartData() + " " +
                    course.getEndData() + " \r\n";

    private Mapper<String, Course> stringToCourseMapper = string -> {
        String data[] = string.split(" ");
        Calendar start = new GregorianCalendar();
        Calendar end = new GregorianCalendar();
        start.set(Calendar.DAY_OF_MONTH, Integer.parseInt(data[2]));
        start.set(Calendar.MONTH, Integer.parseInt(data[3]));
        start.set(Calendar.YEAR, Integer.parseInt(data[4]));
        end.set(Calendar.DAY_OF_MONTH, Integer.parseInt(data[5]));
        end.set(Calendar.MONTH, Integer.parseInt(data[6]));
        end.set(Calendar.YEAR, Integer.parseInt(data[7]));
        return new Course(Long.parseLong(data[0]),
                data[1],
                start,
                end);
    };

    @Override
    public Optional<Course> find(Long key) {
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
            String current = bufferedReader.readLine();

            while (current != null) {
                String data[] = current.split(" ");

                String existedId = data[0];

                // если мы нашли такого пользователя
                if (Long.parseLong(existedId) == key) {
                    Course course = stringToCourseMapper.map(current);
                    List<Long> listTeachersId = searchId(key, "teacherId_courseId.txt");
                    List<Long> listLessonsId = searchId(key, "lessonId_courseId.txt");
                    TeacherDao teacherDao = new TeacherDaoFileBaseImpl(teacherFileName);
                    LessonDao lessonDao = new LessonDaoFileBaseImpl(lessonFileName);
                    List<Lesson> filteredLessons = lessonFilteredById(lessonDao.getAll(), listLessonsId);
                    List<Teacher> filteredTeachers = teacherFilteredById(teacherDao.getAll(), listTeachersId);
                    course.setTeachers(filteredTeachers);
                    course.setLessons(filteredLessons);
                    return Optional.of(course);
                }

                current = bufferedReader.readLine();
            }

            bufferedReader.close();
            return Optional.empty();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    private List<Lesson> lessonFilteredById(List<Lesson> l, List<Long> listid) {
        List<Lesson> result = new ArrayList<>();
        for (Lesson l1 : l
        ) {
            for (Long id : listid
            ) {
                if (l1.getLessonId().equals(id)) {
                    result.add(l1);
                }
            }
        }
        return result;
    }

    private List<Teacher> teacherFilteredById(List<Teacher> l, List<Long> listid) {
        List<Teacher> result = new ArrayList<>();
        for (Teacher l1 : l
        ) {
            for (Long id : listid
            ) {
                if (l1.getTeacherId().equals(id)) {
                    result.add(l1);
                }
            }
        }
        return result;
    }


    @Override
    public void save(Course course) {
        try {
            course.setUUID(idGenerator.nextId());
            writeTeacherId(course);
            writeLessonId(course);
            BufferedWriter outputStream = new BufferedWriter(new FileWriter(fileName, true));
            outputStream.write(courseToStringMapper.map(course));
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public List<Course> getAll() {
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
            String current = bufferedReader.readLine();
            List<Course> courses = new ArrayList<>();

            while (current != null) {
                String data[] = current.split(" ");
                Course course = stringToCourseMapper.map(current);
                courses.add(course);
                current = bufferedReader.readLine();
            }

            bufferedReader.close();
            return courses;
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    /**
     * Считывает ключи из добавленных при создании курса Учителей и формирует список
     * связей для файла fileName
     * в случае если у ключа учителя уже имеется связь с курсом то создает дополнительную
     * строчку в которую добавляет новую связь.
     *
     * @param course ссылка на курс
     */
    private void writeTeacherId(Course course) {
        List<Teacher> teachers = course.getTeachers();
        List<Long> teachersId = getTeachersId(teachers);
        List<String> lines = createLinkId(course, "teacherId_courseId.txt", teachersId, false);
        keyWriter(lines, "teacherId_courseId.txt");
//        course.setTeachers(teachers);
    }

    private void writeLessonId(Course course) {
        List<Lesson> lessons = course.getLessons();
        List<Long> lessonsId = getLessonsId(lessons);
        List<String> lines = createLinkId(course, "lessonId_courseId.txt", lessonsId, true);
        keyWriter(lines, "lessonId_courseId.txt");
//        course.setLessons(lessons);
    }

    /**
     * Перезаписывает файл на основании переданного списка.
     *
     * @param lines
     */
    private void keyWriter(List<String> lines, String fileName) {
        try {
            BufferedWriter keyWriter = new BufferedWriter(new FileWriter(fileName, false));
            for (String s : lines) {
                keyWriter.write(s);
                keyWriter.newLine();
            }
            keyWriter.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Формирует список связей при помощи ключей.
     *
     * @param course
     * @return Список строк для записи в файл.
     */
    private List<String> createLinkId(Course course, String fileName, List<Long> idList, boolean nonChange) {
        List<String> lines = new ArrayList<>();
        try {
            BufferedReader keyReader = new BufferedReader(new FileReader(fileName));

            String k1 = keyReader.readLine();
            String val;
            Long old = -1L;
            while (k1 != null) {

                String[] data = k1.split(" ");
                String existedId = data[0];

                if (chekId(Long.parseLong(existedId), idList)) {
                    if (data.length == 1) {
                        val = existedId + " " + course.getUUID();
                        lines.add(val);
                    } else if (data.length == 2 && !nonChange && old != Long.parseLong(data[1])) {
                        lines.add(k1);
                        lines.add(existedId + " " + course.getUUID());
                        old = Long.parseLong(data[1]);
                    } else {
                        lines.add(k1);
                    }
                } else {
                    lines.add(k1);
                }
                k1 = keyReader.readLine();
            }
            keyReader.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        Set<String> ln = Set.copyOf(lines);
        return List.copyOf(ln);

    }

    /**
     * Возвращает все ключи учителей.
     *
     * @param teachers
     * @return
     */
    private List<Long> getTeachersId(List<Teacher> teachers) {
        List<Long> teachersId = new ArrayList<>();
        for (Teacher teacher : teachers) {
            teachersId.add(teacher.getTeacherId());
        }
        return teachersId;
    }

    private List<Long> getLessonsId(List<Lesson> lessons) {
        List<Long> lessonsId = new ArrayList<>();
        for (Lesson lesson : lessons) {
            lessonsId.add(lesson.getLessonId());
        }
        return lessonsId;
    }

    private boolean chekId(Long parseLong, List<Long> listId) {
        for (Long id : listId
        ) {
            if (id.equals(parseLong)) {
                return true;
            }
        }
        return false;
    }

    private List<Long> searchId(Long id, String fileName) {
        List<Long> listId = new ArrayList<>();
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
            String current = bufferedReader.readLine();

            while (current != null) {
                String data[] = current.split(" ");

                if (data.length == 2) {
                    Long existedId = Long.parseLong(data[1]);
                    if (existedId.equals(id)) {
                        listId.add(Long.parseLong(data[0]));
                    }
                }
                current = bufferedReader.readLine();
            }
            bufferedReader.close();
            return listId;
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }


}