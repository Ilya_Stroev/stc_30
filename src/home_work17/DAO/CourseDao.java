package home_work17.DAO;

import home_work17.model.Course;

public interface CourseDao extends OurDao<Course> {
}
