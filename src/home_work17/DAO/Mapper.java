package home_work17.DAO;

public interface Mapper<X, Y> {
    // преобразует объект типа X в объект типа Y
    Y map(X x);
}
