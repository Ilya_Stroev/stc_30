package home_work17.DAO;

import java.util.List;
import java.util.Optional;

public interface OurDao<T> {
    Optional<T> find(Long key);

    void save(T t);

    List<T> getAll();
}
