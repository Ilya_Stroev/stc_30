package home_work17.DAO;

import home_work17.model.Lesson;

public interface LessonDao extends OurDao<Lesson> {
}
