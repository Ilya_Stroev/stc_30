package home_work17.DAO;

import home_work17.model.Course;
import home_work17.model.Lesson;
import home_work17.model.Teacher;
import home_work17.other.IdGenerator;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class TeacherDaoFileBaseImpl implements TeacherDao {

    private String fileName;

    private IdGenerator idGenerator;

    private String courseFileName;

    public TeacherDaoFileBaseImpl(String fileName, IdGenerator idGenerator, String courseFileName) {
        this(fileName);
        this.idGenerator = idGenerator;
        this.courseFileName = courseFileName;
    }

    public TeacherDaoFileBaseImpl(String fileName) {
        this.fileName = fileName;

    }

    private Mapper<Teacher, String> teacherToStringMapper = teacher ->
            teacher.getTeacherId() + " " +
                    teacher.getFirstName() + " " +
                    teacher.getLastName() + " " +
                    teacher.getExperience() + "\r\n";

    private Mapper<String, Teacher> stringToTeacherMapper = string -> {
        String data[] = string.split(" ");
        return new Teacher(Long.parseLong(data[0]),
                data[1],
                data[2],
                Integer.parseInt(data[3]));
    };

    private List<Long> getCoursesId(Long key) {
        String cur = null;
        List<Long> courseId = new ArrayList<>();
        try {
            BufferedReader readerID = new BufferedReader(new FileReader("teacherId_courseId.txt"));
            cur = readerID.readLine();
            while (cur != null) {
                String[] dat = cur.split(" ");
                if (Long.parseLong(dat[0]) == key) {
                    courseId.add(Long.parseLong(dat[1]));
                }
            }
            readerID.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return courseId;
    }

    @Override
    public Optional<Teacher> find(Long key) {
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
            String current = bufferedReader.readLine();

            while (current != null) {
                String data[] = current.split(" ");

                String existedId = data[0];

                // если мы нашли такого пользователя
                if (Long.parseLong(existedId) == key) {
                    Teacher teacher = stringToTeacherMapper.map(current);
                    List<Long> listId = searchId(key, "teacherId_courseId.txt");
                    CourseDao courseDao = new CourseDaoFileBasedImpl(courseFileName);
                    List<Course> courses = courseFilteredById(courseDao.getAll(), listId);
                    teacher.setCourses(courses);
                    return Optional.of(teacher);
                }

                current = bufferedReader.readLine();
            }

            bufferedReader.close();
            return Optional.empty();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    private List<Course> courseFilteredById(List<Course> l, List<Long> listid) {
        List<Course> result = new ArrayList<>();
        for (Course l1 : l
        ) {
            for (Long id : listid
            ) {
                if (l1.getUUID().equals(id)) {
                    result.add(l1);
                }
            }
        }
        return result;
    }

    @Override
    public void save(Teacher teacher) {
        try {
            BufferedWriter keyTeacher = new BufferedWriter(new FileWriter("teacherId_courseId.txt", true));
            teacher.setTeacherId(idGenerator.nextId());
            keyTeacher.write(teacher.getTeacherId() + " \r\n");
            BufferedWriter outputStream = new BufferedWriter(new FileWriter(fileName, true));
            outputStream.write(teacherToStringMapper.map(teacher));
            outputStream.close();
            keyTeacher.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void setCourses(List<Long> courses, Teacher teacher, CourseDao courseDaoFileBased) {
        List<Course> courses1 = new ArrayList<>();
        for (Long courseId : courses) {
            Optional<Course> cours = courseDaoFileBased.find(courseId);
            cours.ifPresent(courses1::add);

        }
        teacher.setCourses(courses1);
    }

    @Override
    public List<Teacher> getAll() {
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
            String current = bufferedReader.readLine();
            List<Teacher> teachers = new ArrayList<>();

            while (current != null) {
                String data[] = current.split(" ");
                Teacher teacher = stringToTeacherMapper.map(current);
                teachers.add(teacher);
                current = bufferedReader.readLine();
            }

            bufferedReader.close();
            return teachers;
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

    }

    private List<Long> searchId(Long id, String fileName) {
        List<Long> listId = new ArrayList<>();
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
            String current = bufferedReader.readLine();

            while (current != null) {
                String[] data = current.split(" ");

                if (data.length == 2) {
                    Long existedId = Long.parseLong(data[0]);
                    if (existedId.equals(id)) {
                        listId.add(Long.parseLong(data[1]));
                    }
                }
                current = bufferedReader.readLine();
            }
            bufferedReader.close();
            return listId;
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
