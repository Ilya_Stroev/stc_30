package home_work20;

import java.awt.*;
import java.io.IOException;
import java.io.StringBufferInputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {

        try {
            String str = Files.lines(Paths.get("text.txt"))
                    .reduce((a, b) -> a + " " + b)
                    .get();

            List<String> strings = Arrays.stream(str.split("\\s"))
                    .map(word -> word.replaceAll("[!?;:.,]",""))
                    .map(String::toLowerCase)
                    .collect(Collectors.toList());




//            strings = Arrays.asList("3","2","1","5","4");

            quickSort(strings, 0, strings.size() - 1);

            for (String w:strings
                 ) {
                System.out.println(w);
            }



        } catch (IOException e) {
            e.printStackTrace();
        }



    }

    private static List<String> quickSort(List<String> strings, int leftBorder, int rightBorder){
        int leftMarker = leftBorder;
        int rightMarker = rightBorder;
        String pivot = strings.get((leftMarker + rightMarker)/2);

        do{
            while (strings.get(leftMarker).compareTo(pivot) < 0){
                leftMarker++;
            }

            while (strings.get(rightMarker).compareTo(pivot) > 0){
                rightMarker--;
            }

            if (leftMarker <= rightMarker){
                if (leftMarker < rightMarker){
                    String var = strings.get(leftMarker);
                    strings.set(leftMarker,strings.get(rightMarker));
                    strings.set(rightMarker,var);
                }
                leftMarker++;
                rightMarker--;
            }
        }while (leftMarker <= rightMarker);

        if (leftMarker < rightBorder){
            quickSort(strings,leftMarker,rightBorder);
        }
        if(leftBorder < rightMarker){
            quickSort(strings,leftBorder,rightMarker);
        }
        return strings;
    }



}
