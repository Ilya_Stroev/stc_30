package home_work_6;

public class Chanel {

    private int NUM_PROGRAMS = 20;
    private int numChanel;
    private TV tv;
    private Program[] programs;

    public Chanel(int numChanel, TV tv) {
        this.numChanel = numChanel;
        this.tv = tv;
        this.programs = new Program[NUM_PROGRAMS];
        for (int i = 0; i < programs.length; i++) {
            programs[i] = new Program("Program_"+i,this);
        }
    }

    public int getCONST_PROGRAMS() {
        return NUM_PROGRAMS;
    }

    public int getNumChanel() {
        return numChanel;
    }

    public TV getTv() {
        return tv;
    }

    public Program[] getPrograms() {
        return programs;
    }

    public Program getProgram(int numProgram) {
        if(chekNum(numProgram)){
            return programs[numProgram];
        }
        System.out.println("Такой программы нет!");
        return null;
    }

    public Program getProgram(String nameProgram){
        for (int i = 0; i < programs.length; i++) {
            if (programs[i].getName().equals(nameProgram)) {
                return programs[i];
            }
        }
        System.out.println("Программы с таким названием нет!");
        return null;
    }
    private boolean chekNum(int num) {
        return num < NUM_PROGRAMS && num >=0;
    }

    @Override
    public String toString() {
        return "Chanel{" +
                "numChanel=" + numChanel +
                '}';
    }
}
