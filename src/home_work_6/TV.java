package home_work_6;

public class TV {

    private String name;
    private final int NUM_CHANELS = 100;
    private RemoteController remoteController;
    private Chanel[] chanels;

    public TV(String name) {
        this.name = name;
        this.chanels = new Chanel[NUM_CHANELS];
        for (int i = 0; i < chanels.length; i++) {
            chanels[i] = new Chanel(i,this);
        }
    }

    public TV(RemoteController remoteController, String name) {
        this(name);
        this.remoteController = remoteController;


    }

    public Chanel getChanel(int numChanel) {
        if(checkNum(numChanel)){
            return chanels[numChanel];
        }
        return null;
    }
    private boolean checkNum (int num){
        if(num > NUM_CHANELS - 1 || num < 0){
            System.out.println("Такого канала нет!");
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "TV{" +
                "name='" + name + '\'' +
                '}';
    }
}
