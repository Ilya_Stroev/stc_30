package home_work_6;

public class Program {

    private String programName;
    private Chanel chanel;

    public Program(String programName, Chanel chanel) {
        this.programName = programName;
        this.chanel = chanel;
    }

    public String getName() {
        return programName;
    }

    public Chanel getChanel() {
        return chanel;
    }

    @Override
    public String toString() {
        return "Program{" +
                "programName='" + programName + '\'' +
                '}';
    }
}
