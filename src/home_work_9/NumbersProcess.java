package home_work_9;

public interface NumbersProcess {
    int process(int number);
}
