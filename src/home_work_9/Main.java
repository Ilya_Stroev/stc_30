package home_work_9;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {

        int[] arrayNum = {123046, 46350065, 3343, 1304567, 327407, 94734, 9800};
        String[] strings = {"При32вет", "Как дела", "пок6а!"};

        NumbersProcess reverseProcess = number -> {
            int value = number;
            int[] nums = numToArr(value);
            for (int i = 0; i < nums.length / 2; i++) {
                value = nums[i];
                nums[i] = nums[nums.length - i - 1];
                nums[nums.length - i - 1] = value;
            }
            return arrToNum(nums);
        };
        NumbersProcess deleteZero = number -> {
            int value = number;
            int[] nums = numToArr(value);
            int count = nums.length;
            for (int i = 0; i < count; i++) {
                if (nums[i] == 0) {
                    for (int j = i; j < count - 1; j++) {
                        nums[j] = nums[j + 1];
                    }
                    i--;
                    count--;
                }
            }
            nums = Arrays.copyOf(nums, count);
            return arrToNum(nums);
        };
        NumbersProcess notEvenToEven = number -> {
            int value = number;
            int[] nums = numToArr(value);
            for (int i = 0; i < nums.length; i++) {
                if (nums[i] % 2 != 0) {
                    nums[i] = nums[i] - 1;
                }
            }
            return arrToNum(nums);
        };

        StringProcess reverseString = string -> {
            char[] str = string.toCharArray();
            char value;
            for (int i = 0; i < str.length / 2; i++) {
                value = str[i];
                str[i] = str[str.length - i - 1];
                str[str.length - i - 1] = value;
            }

            return String.valueOf(str);
        };

        StringProcess deleteNums = string -> {
            char[] str = string.toCharArray();
            char[] out =  new char[str.length];
            int outIndex = 0;
            for (int i = 0; i < str.length; i++) {
                if (!Character.isDigit(str[i])) {
                    out[outIndex] = str[i];
                    outIndex++;
                }

            }



            return String.valueOf(out, 0, outIndex);
        };

        StringProcess upperCase = string -> string.toUpperCase();

        NumbersAndStringProcessor numProcessor = new NumbersAndStringProcessor(strings, arrayNum);
        int[] val = numProcessor.process((reverseProcess));
        System.out.println(Arrays.toString(arrayNum));
        System.out.println(Arrays.toString(val));
        val = numProcessor.process((deleteZero));
        System.out.println(Arrays.toString(arrayNum));
        System.out.println(Arrays.toString(val));
        val = numProcessor.process((notEvenToEven));
        System.out.println(Arrays.toString(arrayNum));
        System.out.println(Arrays.toString(val));

        String[] str = numProcessor.process((reverseString));
        System.out.println(Arrays.toString(strings));
        System.out.println(Arrays.toString(str));
        str = numProcessor.process((deleteNums));
        System.out.println(Arrays.toString(strings));
        System.out.println(Arrays.toString(str));
        str = numProcessor.process((upperCase));
        System.out.println(Arrays.toString(strings));
        System.out.println(Arrays.toString(str));

    }

    private static int[] numToArr(int num) {
        int value = num;
        int count = 0;
        int[] result;
        do {
            value = value / 10;
            count++;
        } while (value / 10 != 0 || value % 10 != 0);
        result = new int[count];
        value = num;
        for (int i = result.length - 1; i >= 0; i--) {
            result[i] = value % 10;
            value = value / 10;
        }
        return result;
    }

    private static int arrToNum(int[] arr) {
        int value = 0;
        for (int i = 0; i < arr.length; i++) {
            value = value * 10 + arr[i];
        }
        return value;
    }
}
