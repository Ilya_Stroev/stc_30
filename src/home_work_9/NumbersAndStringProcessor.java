package home_work_9;

public class NumbersAndStringProcessor {

    private String[] strings;
    private int[] numbers;

    /**
     *
     * @param strings
     * @param numbers
     */

    public NumbersAndStringProcessor(String[] strings, int[] numbers) {
        this.strings = strings;
        this.numbers = numbers;
    }

    /**
     *
     * @param process
     * @return
     */

    public String[] process(StringProcess process) {
        String[] result = new String[strings.length];
        for (int i = 0; i < strings.length; i++) {
            result[i] = process.process(strings[i]);
        }
        return result;
    }

    /**
     *
     * @param process
     * @return
     */

    public int[] process(NumbersProcess process) {
        int[] result = new int[numbers.length];
        for (int i = 0; i < numbers.length; i++) {
            result[i] = process.process(numbers[i]);
        }
        return result;
    }


}
