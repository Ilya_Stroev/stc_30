package home_work_7;

import com.sun.jdi.event.StepEvent;

public class User {
    private String firstName;
    private String lastName;
    private int age;
    private boolean isWorker;


    public static class UserBuilder {
        private String firstName = "";
        private String lastName = "";
        private int age = 0;
        private boolean isWorker = false;

        public UserBuilder firstName(String firstName) {
            this.firstName = firstName;
            return this;
        }
        public UserBuilder lastName(String lastName) {
            this.lastName = lastName;
            return this;
        }
        public UserBuilder age(int age) {
            this.age = age;
            return this;
        }
        public UserBuilder isWorker(boolean isWorker) {
            this.isWorker = isWorker;
            return this;
        }
        public User build() {
            return new User(this);
        }
    }
        private User (UserBuilder userBuilder) {
            this.firstName = userBuilder.firstName;
            this.lastName = userBuilder.lastName;
            this.age = userBuilder.age;
            this.isWorker = userBuilder.isWorker;
        }
    @Override
    public String toString() {
        return "User{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                ", isWorker=" + isWorker +
                '}';
    }
}
