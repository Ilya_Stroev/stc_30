package home_work19;

import home_work_8.Figure;

import java.io.IOException;
import java.nio.file.Files;

import java.nio.file.Paths;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {

        try {
            /**
             * #1
             */
            Files.lines(Paths.get("cars.txt"))
                    .map(line-> line.split(" "))
                    .map(array -> new Car(array[0],array[1],array[2],Long.parseLong(array[3]),Long.parseLong(array[4])))
                    .filter(car-> car.getColor().equals("Black") || car.getRun().equals(0L))
                    .forEach(car -> System.out.println(car.getNumberCar()));
//
/**
 * #2
 */
            int size = Files.lines(Paths.get("cars.txt"))
                    .map(line -> line.split(" "))
                    .map(array -> new Car(array[0], array[1], array[2], Long.parseLong(array[3]), Long.parseLong(array[4])))
                    .map(Car::getModel)
                    .collect(Collectors.toSet()).size();
            System.out.println(size);
/**
 * #3
 */

            String color = Files.lines(Paths.get("cars.txt"))
                    .map(line -> line.split(" "))
                    .map(array -> new Car(array[0], array[1], array[2], Long.parseLong(array[3]), Long.parseLong(array[4])))
                    .min(Comparator.comparing(Car::getPrice))
                    .get()
                    .getColor();
            System.out.println(color);
            /**
             * #4
             */

            OptionalDouble se = Files.lines(Paths.get("cars.txt"))
                    .map(line -> line.split(" "))
                    .map(array -> new Car(array[0], array[1], array[2], Long.parseLong(array[3]), Long.parseLong(array[4])))
                    .filter(car -> car.getModel().equals("TOYOTA_CAMRY"))
                    .mapToLong(Car::getPrice)
                    .average();
            System.out.println(se);




        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
