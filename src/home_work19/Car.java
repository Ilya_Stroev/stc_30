package home_work19;

import java.util.Collection;
import java.util.Objects;

public class Car implements Comparable<Car> {

    private String numberCar;

    private String model;

    private String color;

    private Long run;

    private Long price;

    public Car(String numberCar, String model, String color, Long run, Long price) {
        this.numberCar = numberCar;
        this.model = model;
        this.color = color;
        this.run = run;
        this.price = price;
    }

    public String getNumberCar() {
        return numberCar;
    }

    public void setNumberCar(String numberCar) {
        this.numberCar = numberCar;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Long getRun() {
        return run;
    }

    public void setRun(Long run) {
        this.run = run;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Car)) return false;
        Car car = (Car) o;
        return Objects.equals(getNumberCar(), car.getNumberCar()) &&
                Objects.equals(getModel(), car.getModel()) &&
                Objects.equals(getColor(), car.getColor()) &&
                Objects.equals(getRun(), car.getRun()) &&
                Objects.equals(getPrice(), car.getPrice());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getNumberCar(), getModel(), getColor(), getRun(), getPrice());
    }


    @Override
    public int compareTo(Car o) {
//        return this.numberCar.hashCode()-o.getNumberCar().hashCode();
        return this.model.hashCode() - o.getModel().hashCode();
    }

    public static int compareByModel(Car o1, Car o2) {
        return o1.getModel().hashCode() - o2.getModel().hashCode();
    }

    @Override
    public String toString() {
        return "Car{" +
                "numberCar='" + numberCar + '\'' +
                ", model='" + model + '\'' +
                ", color='" + color + '\'' +
                ", run=" + run +
                ", price=" + price +
                '}';
    }
}
