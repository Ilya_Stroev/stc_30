package home_work_13;

/**
 * 28.10.2020
 * 19. Collections
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface List<D> extends Collection<D> {
    D get(int index);
    int indexOf(D element);
    void removeByIndex(int index);
    void insert(D element, int index);
    void reverse();
}
