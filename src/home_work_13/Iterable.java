package home_work_13;

/**
 * 28.10.2020
 * 19. Collections
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */

// общий интерфейс, он говорит о том, что можно получить
// объект-итератор

// CREATOR
public interface Iterable<C>{
    // FactoryMethod()
    Iterator<C> iterator();
}
