package home_work_13;

// CONCRETE CREATOR
public class ArrayList<E> implements List<E> {
    // контстанта для начального размера массива
    private static final int DEFAULT_SIZE = 10;
    // поле, представляющее собой массив, в котором мы храним элементы
    private E data[] ;
    // количество элементов в массиве (count != length)
    private int count;

    public ArrayList() {
        this.data = (E[]) new Object[DEFAULT_SIZE];
    }

    private class ArrayListIterator<E> implements Iterator<E> {

        private int current = 0;

        @Override
        public E next() {
            E value = (E) getElement(current);
            current++;
            return value;
        }

        @Override
        public boolean hasNext() {
            return current < count;
        }
    }

    @Override
    public E get(int index) {
        if (index < count) {
            return getElement(index);
        }
        System.err.println("Вышли за пределы массива");// throw new IllegalArgumentException();
        return null;
    }

    @Override
    public int indexOf(E element) {
        for (int i = 0; i < count; i++) {
            if (data[i] == element) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public void removeByIndex(int index) {
        if (index <= count && index >= 0) {
            System.arraycopy(data, index+1,data,index,count - index - 1);
            count --;
        }
    }

    @Override
    public void insert(E element, int index) {
        if (index <= count && index >= 0) {
            if (data.length - 1 == count) {
                resize();
            }
            System.arraycopy(data, index, data, index + 1, count - index);
            data[index] = element;
            count++;
        }
    }

    @Override
    public void reverse() {
        E value;
        for (int i = 0; i < count/2; i++) {
            value = data[i];
            data[i] = data[count -1 -i];
            data[count -1 -i] =value;
        }
    }

    @Override
    public boolean contains(E element) {
        return indexOf(element) != -1;
    }

    @Override
    public void add(E element) {
        if (count == data.length - 1) {
            resize();
        }
        data[count] = element;
        count++;
    }

    private void resize() {
        int oldLength = this.data.length;
        int newLength = oldLength + (oldLength >> 1); // oldLength + oldLength / 2;
        E newData[] = (E[]) new Object[newLength];

        System.arraycopy(this.data, 0, newData, 0, oldLength);

        this.data = newData;
    }


    @Override
    public int size() {
        return this.count;
    }

    @Override
    public void removeFirst(E element) {
        int indexOfRemovingElement = indexOf(element);

        for (int i = indexOfRemovingElement; i < count - 1; i++) {
            this.data[i] = this.data[i + 1];
        }

        this.count--;
    }
     E getElement (int index){
        return this.data[index];
    }

    // реализация метода получения объекта
    @Override
    public Iterator<E> iterator() {
        // CONCRETE PRODUCT
        return new ArrayListIterator<>();
    }



}
