package home_work_13;

/**
 * 28.10.2020
 * 19. Collections
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */

// PRODUCT
public interface Iterator <A> {
    // возвращает следующий элемент
    A next();
    // проверяет, есть ли следующий элемент?
    boolean hasNext();
}
