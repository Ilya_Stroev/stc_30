package home_work_13;

/**
 * 28.10.2020
 * 19. Collections
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// не гарантирует порядок элементов
public interface Collection<B> extends Iterable<B> {
    void add(B element);
    boolean contains(B element);
    int size();
    void removeFirst(B element);
}
