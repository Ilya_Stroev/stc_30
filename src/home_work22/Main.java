package home_work22;

import java.util.Arrays;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Main {

    public static void main(String[] args) throws InterruptedException {


        int[] array = new int[1_000_000];

        Arrays.fill(array, 1);

        int length = array.length;

        Lock lock = new ReentrantLock();

        AtomicInteger startElement = new AtomicInteger(0);

        AtomicInteger atomicInteger = new AtomicInteger(0);


        Runnable task = () -> {
            lock.lock();
            int start = startElement.get();
            int finish = startElement.get() + length / 3;
            if (finish >= length) {
                finish = length - 1;
            }
            startElement.set(finish + 1);
            int val = 0;
            lock.unlock();

            for (int i = start; i <= finish; i++) {
                val += array[i];
//                System.out.println(val);

            }
            atomicInteger.addAndGet(val);
            System.out.println(Thread.currentThread().getName() + ": from " +
                    start + " to " +
                    finish + " sum is " +
                    val);


        };

        Scanner scanner = new Scanner(System.in);
        scanner.nextLine();
        Thread thread1 = new Thread(task);
        Thread thread2 = new Thread(task);
        Thread thread3 = new Thread(task);
        thread1.start();
        thread2.start();
        thread3.start();
        thread1.join();
        thread2.join();
        thread3.join();

        System.out.println("Sum by threads: " + atomicInteger.get());

    }


}
