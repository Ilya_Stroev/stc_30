package home_work_14.result;


/**
 * 07.11.2020
 * 24. Associative Arrays
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class HashMapImpl<K, V> implements Map<K, V> {
    private static final int DEFAULT_SIZE = 16;

    private final MapEntry<K, V>[] entries = new MapEntry[DEFAULT_SIZE];

    private static class MapEntry<K, V> {
        K key;
        V value;
        MapEntry<K, V> next;

        public MapEntry(K key, V value) {
            this.key = key;
            this.value = value;
        }
    }

    // TODO: предусмотреть ЗАМЕНУ ЗНАЧЕНИЯ
    // put("Марсель", 27);
    // put("Марсель", 28);
    // у меня сохранятся оба значения, вам нужно сделать, чтобы хранилось только одно
    @Override
    public void put(K key, V value) {
        MapEntry<K, V> newMapEntry = new MapEntry<>(key, value);
        int index = key.hashCode() & (entries.length - 1);
        if (entries[index] == null) {
            entries[index] = newMapEntry;
        } else {
            // если уже в этой ячейке (bucket) уже есть элемент
            // запоминяем его
            MapEntry<K, V> current = entries[index];

            // теперь дойдем до последнего элемента в этом списке
            if (!checkKey(current, newMapEntry.key)) {
                while (current.next != null) {
                    current = current.next;
                    if (checkKey(current, newMapEntry.key)) {
                        current.value = newMapEntry.value;
                        return;
                    }
                }
            } else {
                current.value = newMapEntry.value;
                return;
            }
            // теперь мы на последнем элементе, просто добавим новый элемент в конец
            current.next = newMapEntry;
        }
    }

    // TODO: реализовать
    @Override
    public V get(K key) {
        MapEntry<K, V> current;
        for (MapEntry<K, V> entry : entries) {
            current = entry;
            if (current != null) {
                if (!checkKey(current, key)) {
                    while (current.next != null) {
                        current = current.next;
                        if (checkKey(current, key)) {
                            return current.value;
                        }
                    }
                } else {
                    return current.value;
                }
            }
        }

        return null;
    }

    private boolean checkKey(MapEntry<K, V> current, K key) {
        return current.key.equals(key);
    }
}
