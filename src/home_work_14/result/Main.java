package home_work_14.result;



/**
 * 07.11.2020
 * 24. Associative Arrays
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
//        Map<String, Integer> map = new HashMapImpl<>();
//
//        map.put("Марсель", 26);
//        map.put("Денис", 30);
//        map.put("Илья", 28);
//        map.put("Неля", 18);
//        map.put("Катерина", 23);
//        map.put("Полина", 18);
//        map.put("Регина", 18);
//        map.put("Максим", 18);
//        map.put("Сергей", 18);
//        map.put("Иван", 18);
//        map.put("Виктор", 18);
//        map.put("Виктор Александрович", 18);
//        map.put("Илья", 26);
//        map.put("Виктор Александрович", 98);
//
//        System.out.println(map.get("Марсель"));
//        System.out.println(map.get("Денис"));
//        System.out.println(map.get("Илья"));
//        System.out.println(map.get("Неля"));
//        System.out.println(map.get("Катерина"));
//        System.out.println(map.get("Полина"));
//        System.out.println(map.get("Регина"));
//        System.out.println(map.get("Виктор Александрович"));
//        System.out.println(map.get("sdak"));




        Set<String> set = new HashSetImpl<>();

        set.add("Марсель" );
        set.add("Денис");
        set.add("Илья");
        set.add("Неля");
        set.add("Катерина");
        set.add("Полина");
        set.add("Регина");
        set.add("Максим");
        set.add("Сергей");
        set.add("Иван");
        set.add("Виктор");
        set.add("Виктор Александрович");
        set.add("Илья");
        set.add("Виктор Александрович");

        set.contains("Марсель");
        set.contains("Денис");
        set.contains("Илья");
        set.contains("Неля");
        set.contains("Катерина");
        set.contains("Полина");
        set.contains("Регина");
        set.contains("Виктор Александрович");
        set.contains("sdak");

        System.out.println(set.contains("Марсель"));
        System.out.println(set.contains("Денис"));
        System.out.println(set.contains("Илья"));
        System.out.println(set.contains("Неля"));
        System.out.println(set.contains("Катерина"));
        System.out.println(set.contains("Полина"));
        System.out.println(set.contains("Регина"));
        System.out.println(set.contains("Виктор Александрович"));
        System.out.println(set.contains("sdak"));

        int i = 0;
    }
}
