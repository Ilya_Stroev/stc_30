package home_work_21;

public class Main {

    public static void main(String[] args) {
        BinarySearchTree<Integer> tree = new BinarySearchTreeImpl<>();
        tree.insert(8);
        tree.insert(3);
        tree.insert(10);
        tree.insert(1);
        tree.insert(6);
        tree.insert(14);
        tree.insert(4);
        tree.insert(7);
        tree.insert(13);

        tree.printBfs();
        tree.printLineBfs();
//        tree.printDfsByStack();
//        tree.remove(13);
//        tree.printBfs();
//        System.out.println(tree.contains(8));
//        System.out.println(tree.contains(13));
//        System.out.println(tree.contains(6));
//        System.out.println(tree.contains(2));
    }
}
