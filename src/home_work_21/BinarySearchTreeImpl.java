package home_work_21;

import org.w3c.dom.Node;

import java.util.Deque;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;

/**
 * 02.12.2020
 * 32. Trees
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class BinarySearchTreeImpl<T extends Comparable<T>> implements BinarySearchTree<T> {

    static class Node<E> {
        E value;
        Node<E> left;
        Node<E> right;

        public Node(E value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return value.toString();
        }
    }

    private Node<T> root;

    @Override
    public void insert(T value) {
        this.root = insert(this.root, value);
    }

    private Node<T> insert(Node<T> root, T value) {
        if (root == null) {
            root = new Node<>(value);
        }
        // значение меньше корня
        else if (value.compareTo(root.value) < 0) {
            // добавляем в левое поддерево
            root.left = insert(root.left, value);
        } else {
            root.right = insert(root.right, value);
        }

        return root;
    }

    @Override
    public void printDfs() {
        dfs(root);
    }

    @Override
    public void printDfsByStack() {
        Deque<Node<T>> stack = new LinkedList<>();
        stack.push(root);

        Node<T> current;

        while (!stack.isEmpty()) {
            current = stack.pop();
            System.out.println(current.value + " ");
            if (current.left != null) {
                stack.push(current.left);
            }
            if (current.right != null) {
                stack.push(current.right);
            }
        }
    }

    @Override
    public void printBfs() {
        Deque<Node<T>> queue = new LinkedList<>();
        queue.add(root);

        Node<T> current;

        while (!queue.isEmpty()) {
            current = queue.poll();
            System.out.println(current.value + " ");
            if (current.left != null) {
                queue.add(current.left);
            }
            if (current.right != null) {
                queue.add(current.right);
            }
        }
    }

    @Override
    public void printLineBfs() {
        Queue<Node<T>> currentLevel = new LinkedList<>();
        Queue<Node<T>> nextLevel = new LinkedList<>();

        currentLevel.add(root);
        Node<T> current;

        while (!currentLevel.isEmpty()) {
            for (Node<T> tNode : currentLevel) {
                current = tNode;
                if (current.left != null) {
                    nextLevel.add(current.left);
                }
                if (current.right != null) {
                    nextLevel.add(current.right);
                }
                System.out.print(current.value + " ");

            }
            System.out.println();
            currentLevel = nextLevel;
            nextLevel = new LinkedList<>();
        }
    }

    private void dfs(Node<T> root) {
        System.out.println("in root = " + root);
        if (root != null) {
            dfs(root.left);
            System.out.println(root.value + " ");
            dfs(root.right);
        }
        System.out.println("from root = " + root);
    }

    @Override
    public void remove(T value) {
        remove(root, value);
    }

    private void remove(Node<T> tNode, T value) {

        if (tNode.value == value) {
            if (tNode.right != null) {
                searchRightAndPaste(tNode.right, tNode.left);

                searchAndPasteParent(tNode.right, value);
            } else if (tNode.left != null) {
                searchAndPasteParent(tNode.left, value);
            } else {
                searchAndRemoveLeaf(value);
            }
        } else {
            if (tNode.value.compareTo(value) > 0) {
                remove(tNode.left, value);
            } else {
                remove(tNode.right, value);
            }
        }
    }

    private void searchRightAndPaste(Node<T> root, Node<T> left) {
        if (root.left == null) {
            root.left = left;
        } else {
            searchRightAndPaste(root.left, left);
        }
    }

    private void searchAndPasteParent(Node<T> root, T value) {
        Deque<Node<T>> queue = new LinkedList<>();
        queue.add(this.root);

        Node<T> current;

        while (!queue.isEmpty()) {
            current = queue.poll();

//            System.out.println(current.value + " ");
            if (current.left != null) {
                if (current.left.value == value) {
                    current.left = root;
                    return;
                }
                queue.add(current.left);
            }
            if (current.right != null) {
                if (current.right.value == value) {
                    current.right = root;
                    return;
                }
                queue.add(current.right);
            }
        }
        if (this.root.value == value) {
            this.root = root;
        }
    }

    private void searchAndRemoveLeaf(T value) {
        Deque<Node<T>> queue = new LinkedList<>();
        queue.add(this.root);

        Node<T> current;

        while (!queue.isEmpty()) {
            current = queue.poll();

//            System.out.println(current.value + " ");
            if (current.left != null) {
                if (current.left.value == value) {
                    current.left = null;
                    return;
                }
                queue.add(current.left);
            }
            if (current.right != null) {
                if (current.right.value == value) {
                    current.right = null;
                    return;
                }
                queue.add(current.right);
            }
        }
    }

    @Override
    public boolean contains(T value) {
        Deque<Node<T>> queue = new LinkedList<>();
        queue.add(this.root);

        Node<T> current;

        while (!queue.isEmpty()) {
            current = queue.poll();
            if (current.value == value) {
                return true;
            }
//            System.out.println(current.value + " ");
            if (current.left != null) {
                queue.add(current.left);
            }
            if (current.right != null) {
                queue.add(current.right);
            }
        }
        return false;
    }
}
