package home_work_11;


public class LinkedList implements List {

    private class LinkedListIterator implements Iterator {

        int current = 0;

        @Override
        public int next() {
            int value = get(current);
            current++;
            return value;
        }

        @Override
        public boolean hasNext() {
            return get(current) != -1;
        }
    }

    private Node first;
    private Node last;

    private int count;

    private static class Node {
        int value;
        Node next;

        public Node(int value) {
            this.value = value;
        }
    }

    @Override
    public int get(int index) {
        if (index >= 0 && index < count && first != null) {
            int i = 0;
            Node current = this.first;

            while (i < index) {
                current = current.next;
                i++;
            }

            return current.value;
        }
        System.err.println("Такого элемента нет");
        return -1;


    }

    @Override
    public int indexOf(int element) {
        int i = 0;
        Node current = this.first;

        while (current != null && current.value != element) {
            current = current.next;
            i++;
        }

        if (current == null) {
            return -1;
        } else {
            return i;
        }
    }

    @Override
    public void removeByIndex(int index) {

        Node current = this.first;
        Node current1 = current.next;
        if (index < count && index > 0) {
            for (int j = 0; j < index - 1; j++) {
                current = current.next;
                current1 = current.next;
            }
            current.next = current1.next;
            count--;
        } else if (index == 0) {
            this.first = current.next;
            count--;
        }
    }

    @Override
    public void insert(int element, int index) {
        Node current = this.first;
        Node current1 = current.next;
        Node newNode = new Node(element);
        if (index <= count && index > 0) {
            for (int j = 0; j < index - 1; j++) {
                current = current.next;
                current1 = current.next;
            }
            current.next = newNode;
            newNode.next = current1;
            count++;
        } else if (index == 0) {
            newNode.next = first;
            first = newNode;
            count++;
        } else {
            System.out.println("Данного индекса нет!");
        }

    }

    @Override
    public void reverse() {
        Node current = this.first;
        Node[] list = new Node[count];
        for (int i = count-1; i >=0 ; i--) {
            list[i] = current;
            current = current.next;
        }

        for (int i = 0; i < count-1; i++) {
            insert(list[i].value,i);
            count--;//это не баг а фича))
        }
    }

    @Override
    public void add(int element) {
        Node newNode = new Node(element);
        if (first == null) {
            first = newNode;
            last = newNode;
        } else {

            last.next = newNode;

            last = newNode;
        }
        count++;
    }

    @Override
    public boolean contains(int element) {
        return indexOf(element) >= 0;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public void removeFirst(int element) {
        Node current = this.first;
        Node current1 = current.next;


        while (current.next != null && current1.value != element) {
            current = current.next;
            current1 = current1.next;
        }
        if (first.value == element){
            first = first.next;
            count--;
        } else if (current1 != null && current1.value == element) {
            current.next = current1.next;
            count--;
        }


    }

    @Override
    public Iterator iterator() {
        return new LinkedListIterator();
    }
}
