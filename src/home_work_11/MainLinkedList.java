package home_work_11;


public class MainLinkedList {

    public static void main(String[] args) {
        List list = new LinkedList();

        list.add(777);
        list.add(888);
        list.add(555);
        list.add(999);
        list.add(888);
        list.add(555);
        list.add(111);

//        System.out.println(list.contains(222));
//        list.removeFirst(111);
//        list.removeByIndex(2);
//        list.insert(111,0);

        Iterator iterator = list.iterator();

        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }

        list.reverse();
        iterator = list.iterator();

        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }

//        System.out.println(list.get(0));
//        System.out.println(list.get(1));
//        System.out.println(list.get(2));
//        System.out.println(list.get(3));

    }
}
