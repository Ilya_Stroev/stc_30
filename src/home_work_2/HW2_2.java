package home_work_2;

import java.util.Scanner;
import java.util.Arrays;

class HW2_2 {

public static void main(String args[]) {
Scanner scanner = new Scanner(System.in);
System.out.print("Enter the number of numbers: ");
int n = scanner.nextInt();
int array[] = new int[n];
for (int i= array.length -1; i >= 0; i-- ) {
	System.out.print("Enter the number: ");
	array[i] = scanner.nextInt();	
	}
System.out.println(Arrays.toString(array)); // выводится массив в зеркальном представлении
}
}