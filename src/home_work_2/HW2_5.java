package home_work_2;

import java.util.Scanner;
import java.util.Arrays;

class HW2_5 {
public static void main(String args[]) {
Scanner scanner = new Scanner(System.in);
System.out.print("Enter the number of numbers: ");
int n = scanner.nextInt();
int array[] = new int[n];
int numChange;
int countChange;

	for (int i =0; i < array.length; i++ ) {
		System.out.print("Enter the number: ");
		array[i] = scanner.nextInt();	
	}

	System.out.println(Arrays.toString(array));

	do{
		countChange = 0;
		for (int i = 0; i < array.length-1; i++ ) {
			 if(array[i] > array[i+1]){
			 	numChange = array[i];
			 	array[i] = array[i+1];
			 	array[i+1] = numChange;
			 	countChange++;
			 }
		}
	} while(countChange != 0);
	
System.out.println(Arrays.toString(array));
}
}