package home_work_8;

public class Circle extends Ellipse {



    public Circle(int x, int y, float radius) {
        super(x, y,radius, radius);
    }

    @Override
    public float getPerimeter() {
        return (float) (Math.PI * radius * radius);
    }


}
