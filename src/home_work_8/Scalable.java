package home_work_8;

public interface Scalable {

    public void scaleUpIn(int val);

    public void scaleDnIn(int val);
}
