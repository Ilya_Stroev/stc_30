package home_work_8;

public interface Relocatable {
    public void relocate(int x, int y);
}
