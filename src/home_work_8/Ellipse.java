package home_work_8;

public class Ellipse extends Figure {

    protected float radius;
    private float radius2;

    public Ellipse(int x, int y, float radius, float radius2) {
        super(x, y);
        this.radius = radius;
        this.radius2 = radius2;
    }

    public float getRadius() {
        return radius;
    }

    public float getRadius2() {
        return radius2;
    }

    @Override
    public void scaleUpIn(int val) {
        radius = radius * val;
        radius2 = radius2 * val;
    }

    @Override
    public void scaleDnIn(int val) {
        radius = radius / val;
        radius2 = radius2 / val;
    }

    @Override
    public float getArea() {
        return (float) Math.PI * radius * radius2;
    }

    @Override
    public float getPerimeter() {
        return (float) (4 * Math.PI * radius * radius2 + Math.pow((radius - radius2),2))/ radius + radius2;
    }
}
