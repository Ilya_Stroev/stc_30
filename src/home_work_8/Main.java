package home_work_8;

public class Main {
    public static void main(String[] args) {
        Figure f1 = new Circle(10,10,5);
        Figure f2 = new Square(20,10,5);
        Figure f3 = new Rectangle(10,20,5,10);
        Figure f4 = new Ellipse(20 , -10,10,5);


        System.out.println(f1.getArea());
        System.out.println(f1.getPerimeter());
        f1.scaleUpIn(2);
        Circle circle = (Circle) f1;
        System.out.println(circle.getRadius());
        System.out.println(f1.getArea());
        System.out.println(f1.getPerimeter());
        f1.relocate(75, -15);
        System.out.println("f1 X-> " + f1.getX() + " " + "f1 Y-> " + f1.getY());

        System.out.println("f1 X-> " + f4.getX() + " " + "f1 Y-> " + f4.getY());
        System.out.println(f4.getArea());
        System.out.println(f4.getPerimeter());
        f4.scaleUpIn(2);
        Ellipse ellipse = (Ellipse) f4;
        System.out.println(ellipse.getRadius() + " " + ellipse.getRadius2());
        System.out.println(f4.getArea());
        System.out.println(f4.getPerimeter());
        f4.relocate(75, -15);
        System.out.println("f1 X-> " + f4.getX() + " " + "f1 Y-> " + f4.getY());



    }
}
