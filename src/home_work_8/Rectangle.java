package home_work_8;

public class Rectangle extends Figure {

    private float length;
    private float width;

    public Rectangle(int x, int y, float length, float width) {
        super(x, y);
        this.length = length;
        this.width = width;
    }

    public float getLength() {
        return length;
    }

    public float getWidth() {
        return width;
    }

    @Override
    public float getArea() {
        return length * width;
    }

    @Override
    public float getPerimeter() {
        return 2 * (length + width);
    }

    @Override
    public void scaleUpIn(int val) {
        length = length * val;
        width = width * val;

    }

    @Override
    public void scaleDnIn(int val) {
        length = length / val;
        width = width / val;
    }
}
