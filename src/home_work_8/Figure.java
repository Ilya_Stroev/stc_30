package home_work_8;

public abstract class Figure implements Scalable, Relocatable {

    private int centerX;
    private int centerY;
    private float area;
    private float perimeter;

    public Figure(int x, int y) {
        this.centerX = x;
        this.centerY = y;
    }

    public int getX() {
        return this.centerX;
    }

    public int getY() {
        return this.centerY;
    }

    public abstract float getArea();

    public abstract float getPerimeter();

    @Override
    public void relocate(int x, int y) {
        this.centerX = x;
        this.centerY = y;
    }
}
