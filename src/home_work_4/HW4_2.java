package home_work_4;

public class HW4_2 {

	public static void main (String args[]) {
		int arr[] = {1, 2, 3, 4, 5};
		printResult(binarySearch(arr, 1));
		printResult(binarySearch(arr, 6));
		printResult(binarySearch(arr, 5));
		printResult(binarySearch(arr, 4));

	}

	public static void printResult (int i) {
		if (i == 1) {
			System.out.println("yes");
		}
		else {
			System.out.println("no");
		}

	}
	public static int binarySearch(int array[], int element) {

		if((array[array.length/2] != element) && (array.length <= 1)){
			return 0;
		}
		else if(array[array.length/2] > element) {
			array = addArray(0, array.length/2,array);
			return binarySearch(array,element);
		}
		else if(array[array.length/2] < element) {
			array = addArray(array.length/2,array.length,array);
			return binarySearch(array,element);
		}

 		return 1;
	}
	public static int[] addArray(int first, int last, int array[]){
		int arr [] = new int[last - first];
		for (int i = 0; i < arr.length; i++) {
			arr[i] = array[first + i];
		}
		return arr;
	}
                    
	
}